<?php
include("includes/connect.php");

$year = 0;
$month = 0;
$data = mysqli_query($con, "SHOW TABLES");
while ($row = mysqli_fetch_assoc($data)) {
  $name = $row['Tables_in_4GUsage'];
  if ($name == "mainData" || $name == "faults" || $name == "alerts")
    continue;

  $full = explode("_", $name);
  if ($full[0] > $year)
    $year = $full[0];
  if ($full[1] > $month)
    $month = $full[1];
}

$path = explode("/", $_SERVER['PHP_SELF']);
$link = 'http://'.$_SERVER['HTTP_HOST'] . "/" . $path[1] . "/showData.php?year=" . $year . "&month=" . $month;
header("Location: " . $link);
?>
