<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Data Usage | Alerts</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="includes/styles.css">
  </head>
  <body>
    <?php
    include("includes/navbar.php");
    include("includes/connect.php")
    ?>

    <center>
      <div class="container" style="padding-top: 10px;">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">
                Alerts
                <div style="padding-left: 77%;" class="btn-group" role="group">
                  <a href="addAlert.php">
                    <button type="button" class="btn btn-primary">Add An Alert</button>
                  </a>
                </div>
              </h3>
            </div>
            <div class="panel-body">
              <table width="100%" class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th>Email</th>
                    <th>Amount</th>
                    <th>&nbsp;</th>
                 </thead>
                 <?php
                  $data = mysqli_query($con, "SELECT * FROM alerts");
                  while ($row = mysqli_fetch_assoc($data)) {
                    $email = $row['email'];
                    $amount = $row['amount'];

                    echo "<tr>
                    <td width='25%'>" . $email . "</td>
                    <td width='25%'>" . $amount . "</td>
                    <td style='float:right; padding-right:20%;'>
                      <a style='padding-right:2px;' href='editAlert.php?id=" . $row['ID'] . "'>
                        <img class='actionBtn' width=16 height=16 src='includes/images/editIcon.png'>
                      </a>
                      <a style='padding-left:2px;' href='deleteAlert.php?id=" . $row['ID'] . "'>
                        <img class='actionBtn' width=16 height=16 src='includes/images/deleteIcon.svg'>
                      </a>
                    </td>
                    </tr>";
                  }
                 ?>
                </table>
            </div>
          </div>
        </div>
    </center>

  <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
</html>
