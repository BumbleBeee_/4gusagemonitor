<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Add An Alert</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script></head>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="includes/styles.css">
  </head>
  <body>
    <?php
    include("includes/connect.php");
    include("includes/navbar.php");
    ?>
    <div class="container" style="padding-top: 10px;">
      <?php
      if (isset($_POST['submit'])) {
        $email = $_POST['email'];
        $amount = $_POST['amount'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
          echo '<center><div class="alert alert-danger" role="alert">Error: Invalid email address!</div></center>';
        } else {
          $query = "INSERT INTO alerts (email,amount)
                      VALUES ('$email', '$amount')";

  	      if (mysqli_query($con, $query) === FALSE) {
            echo '<center><div class="alert alert-danger" role="alert">Error: Failed to add alert<br>'.mysqli_error($con).'</div></center>';
          } else {
            echo '<center><div class="alert alert-success" role="alert">Successfully added alert</div></center>';
          }
        }
      }
      ?>
      <div class="col-md-2">
      </div>
      <div class="col-md-8">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              Add An Alert
            </h3>
          </div>
          <div class="panel-body">
            <form action="addAlert.php" method="POST" enctype="multipart/form-data">

              <label for="email">Email:&nbsp;</label><input type="text" name="email">
              <br><br>
              <label for="amount">Amount (GB):&nbsp;</label><input type="number" name="amount">
              <br><br>

              <center><input type="submit" name="submit" value="Submit"></center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
